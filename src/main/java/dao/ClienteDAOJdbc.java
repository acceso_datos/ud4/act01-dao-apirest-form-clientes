package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;

/**
 *
 * @author sergio
 */
public class ClienteDAOJdbc implements GenericDAO<Cliente> {
	final static String TABLA = "clientes";
	final static String PK = "id";

	final String SQLSELECTALL = "SELECT * FROM " + TABLA;
	final String SQLSELECTCOUNT = "SELECT count(*) FROM " + TABLA;
	final String SQLSELECTPK = "SELECT * FROM " + TABLA + " WHERE " + PK + " = ?";
	final String SQLSELECTEXAMPLE = "SELECT * FROM " + TABLA + " where nombre like ? and direccion like ?";
	final String SQLINSERT = "INSERT INTO " + TABLA + " (nombre, direccion) VALUES (?, ?)";
	final String SQLUPDATE = "UPDATE " + TABLA + " SET nombre = ?, direccion = ? WHERE " + PK + " = ?";
	final String SQLDELETE = "DELETE FROM " + TABLA + " WHERE " + PK + " = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private final PreparedStatement pstSelectCount;
	private final PreparedStatement pstSelectExample;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;

	public ClienteDAOJdbc() throws SQLException {
		Connection con = ConexionBD.getConexion();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectCount = con.prepareStatement(SQLSELECTCOUNT);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstSelectExample = con.prepareStatement(SQLSELECTEXAMPLE);
		pstInsert = con.prepareStatement(SQLINSERT, PreparedStatement.RETURN_GENERATED_KEYS);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}

	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstSelectCount.close();
		pstSelectExample.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	private Cliente build(int id, String nombre, String direccion) {
		return new Cliente(id, nombre, direccion);
	}
	
	public Cliente find(int id) throws SQLException {
		Cliente c = null;
		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();
		if (rs.next()) {
			return build(id, rs.getString("nombre"), rs.getString("direccion"));
		}
		rs.close();
		return c;
	}

	public List<Cliente> findAll() throws SQLException {
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		ResultSet rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaClientes.add(build(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
		}
		rs.close();
		return listaClientes;
	}

	public boolean insert(Cliente cliInsertar) throws SQLException {
		pstInsert.setString(1, cliInsertar.getNombre());
		pstInsert.setString(2, cliInsertar.getDireccion());
		int insertados = pstInsert.executeUpdate();
		if (insertados == 1) {
			ResultSet rsClave = pstInsert.getGeneratedKeys();
			rsClave.next();
			cliInsertar.setId(rsClave.getInt(1));
			rsClave.close();
			return true;
		}
		return false;
	}

	public boolean update(Cliente cliActualizar) throws SQLException {
		pstUpdate.setString(1, cliActualizar.getNombre());
		pstUpdate.setString(2, cliActualizar.getDireccion());
		pstUpdate.setInt(3, cliActualizar.getId());
		int actualizados = pstUpdate.executeUpdate();
		return (actualizados == 1);
	}

	public boolean delete(int id) throws SQLException {
		pstDelete.setInt(1, id);
		int borrados = pstDelete.executeUpdate();
		return (borrados == 1);
	}

	public boolean delete(Cliente cliEliminar) throws SQLException {
		return this.delete(cliEliminar.getId());
	}

	public List<Cliente> findByExample(Cliente muestra) throws SQLException {
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		String filtroNombre = muestra.getNombre() == null ? "%" : "%" + muestra.getNombre() + "%";
		pstSelectExample.setString(1, filtroNombre);
		String filtroDireccion = muestra.getDireccion() == null ? "%" : "%" + muestra.getDireccion() + "%";
		pstSelectExample.setString(2, filtroDireccion);
		ResultSet rs = pstSelectExample.executeQuery();
		while (rs.next()) {
			listaClientes.add(build(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
		}
		rs.close();
		return listaClientes;
	}

}
